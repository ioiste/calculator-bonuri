document.addEventListener('DOMContentLoaded', function () {
    const params = new Proxy(new URLSearchParams(window.location.search), {
        get: (searchParams, prop) => searchParams.get(prop),
    });
    window.person1Name = params.p1 ?? 'Persoana 1';
    window.person2Name = params.p2 ?? 'Persoana 2';

    const person1NameElements = document.getElementsByClassName('person1-name');
    const person2NameElements = document.getElementsByClassName('person2-name');

    for (const key in person1NameElements) {
        person1NameElements[key].innerText = window.person1Name;
    }

    for (const key in person2NameElements) {
        person2NameElements[key].innerText = window.person2Name;
    }
    changeWhoPaysText();
});

document.addEventListener('input', function () {
    calculate();
});

function addInput(listID) {
    const ul = document.getElementById(listID);
    const li = document.createElement('li');

    const divInputGroup = document.createElement('div');
    divInputGroup.className = 'input-group';

    const divCol8 = document.createElement('div');
    divCol8.className = 'col-8';

    const inputVal = document.createElement('input');
    inputVal.type = 'number';
    inputVal.className = 'form-control input-value';

    divCol8.appendChild(inputVal);

    const divCol4 = document.createElement('div');
    divCol4.className = 'col-4';

    const inputQty = document.createElement('input');
    inputQty.type = 'number';
    inputQty.className = 'form-control input-qty';
    inputQty.value = '1';

    divCol4.appendChild(inputQty);

    divInputGroup.appendChild(divCol8);
    divInputGroup.appendChild(divCol4);

    li.appendChild(divInputGroup);
    ul.appendChild(li);

    this.calculate();
}

function changeWhoPaysText() {
    const whoPaysWhoElement = this.getWhoPaysWhoElement();
    whoPaysWhoElement.textContent = this.getWhoPaid() === 'person1' ? window.person2Name : window.person1Name;
    whoPaysWhoElement.classList.remove('person1-name', 'person2-name')
    whoPaysWhoElement.classList.add(this.getWhoPaid() === 'person1' ? 'person1-name' : 'person2-name')

    const whoPaidElement = this.getWhoPaidElement();
    whoPaidElement.textContent = this.getWhoPaid() === 'person1' ? window.person1Name : window.person2Name;
    whoPaidElement.classList.remove('person1-name', 'person2-name')
    whoPaidElement.classList.add(this.getWhoPaid() + '-name')

    this.calculate();
}

function getWhoPaysWhoElement() {
    return document.getElementById('whoPaysText');
}

function getWhoPaidElement() {
    return document.getElementById('whoPaidText');
}

function getWhoPaid() {
    const paidByRadios = document.getElementsByName('paid-by');
    for (let i = 0; i < paidByRadios.length; i++) {
        if (paidByRadios[i].checked) {
            return paidByRadios[i].value;
        }
    }
}

function calculate() {
    const total = parseFloat(document.getElementById('total').value);
    const [person1ListSum, person1ListCalculusExplanation] =
        sumListWithExplanations(document.getElementById('person1-list'));
    const [person2ListSum, person2ListCalculusExplanation] =
        sumListWithExplanations(document.getElementById('person2-list'));
    const commonAmount = (total - person1ListSum - person2ListSum) / 2;

    let amountToPay = commonAmount;
    amountToPay += getWhoPaid() === 'person1' ? person2ListSum : person1ListSum;

    const explanationElement = document.getElementById('explanation');
    explanationElement.innerHTML = '';

    const person1ListCalculusElement = document.createElement('div');
    person1ListCalculusElement.textContent = `Calcul ${window.person1Name}:`;
    explanationElement.appendChild(person1ListCalculusElement);
    const person1ListCalculusList = document.createElement('ul');
    for (let i = 0; i < person1ListCalculusExplanation.length; i++) {
        const listItem = document.createElement('li');
        listItem.textContent = person1ListCalculusExplanation[i];
        person1ListCalculusList.appendChild(listItem);
    }
    person1ListCalculusElement.appendChild(person1ListCalculusList);

    const person2ListCalculusElement = document.createElement('div');
    person2ListCalculusElement.textContent = `Calcul ${window.person2Name}:`;
    explanationElement.appendChild(person2ListCalculusElement);
    const person2ListCalculusList = document.createElement('ul');
    for (let i = 0; i < person2ListCalculusExplanation.length; i++) {
        const listItem = document.createElement('li');
        listItem.textContent = person2ListCalculusExplanation[i];
        person2ListCalculusList.appendChild(listItem);
    }
    person2ListCalculusElement.appendChild(person2ListCalculusList);

    const commonAmountElement = document.createElement('div');
    commonAmountElement.textContent
        = `La comun: ${total} - ${person1ListSum} - ${person2ListSum} = ${commonAmount * 2}`;
    explanationElement.appendChild(commonAmountElement);
    const commonAmountList = document.createElement('ul');
    const commonAmountItem1 = document.createElement('li');
    commonAmountItem1.textContent = `Adică ${commonAmount * 2} / 2 = ${commonAmount} fiecare`;
    commonAmountList.appendChild(commonAmountItem1);
    commonAmountElement.appendChild(commonAmountList);

    const personPaidElement = document.createElement('div');
    const personToPayList = createPersonToPayList(commonAmount, amountToPay, person1ListSum, person2ListSum);
    if (getWhoPaid() === 'person1') {
        personPaidElement.textContent = `${window.person1Name} a plătit`;
        personToPayList.classList.add('person2-to-pay');
    } else {
        personPaidElement.textContent = `${window.person2Name} a plătit`;
        personToPayList.classList.add('person1-to-pay');
    }
    explanationElement.appendChild(personPaidElement);
    personPaidElement.appendChild(personToPayList);

    const amountToPayElement = document.getElementById('amountToPay');
    if (isNumeric(amountToPay)) {
        amountToPayElement.innerText = amountToPay.toString();
        explanationElement.hidden = false;
        const amountPaidElement = document.getElementById('amountPaid');
        amountPaidElement.innerText = (total - amountToPay).toString();
    } else {
        amountToPayElement.innerText = '?';
        explanationElement.hidden = true;
    }
}

function createPersonToPayList(commonAmount, amountToPay, person1ListSum, person2ListSum) {
    const personToPayList = document.createElement('ul');
    const personToPayItem1 = document.createElement('li');

    if (getWhoPaid() === 'person1') {
        personToPayItem1.textContent = `${window.person2Name} are de plătit ${commonAmount} + ${person2ListSum} = ${amountToPay}`;
    } else {
        personToPayItem1.textContent = `${window.person1Name} are de plătit ${commonAmount} + ${person1ListSum} = ${amountToPay}`;
    }
    personToPayList.appendChild(personToPayItem1);

    return personToPayList;
}

function sumListWithExplanations(personList) {
    const inputGroups = personList.querySelectorAll('.input-group');
    let totalSum = 0;
    const explanations = [];

    inputGroups.forEach((inputGroup, index) => {
        const valueInput = inputGroup.querySelector('.input-value');
        const qtyInput = inputGroup.querySelector('.input-qty');
        const value = valueInput.value ? parseFloat(valueInput.value) : 0;
        const qty = qtyInput.value ? parseInt(qtyInput.value) : 0;
        const rowTotal = value * qty;
        totalSum += rowTotal;
        explanations.push(`Rând ${index + 1}: ${value} * ${qty} = ${rowTotal}`);
    });
    explanations.push(`Total: ${totalSum}`);

    return [totalSum, explanations];
}

function isNumeric(str) {
    return !isNaN(str) && !isNaN(parseFloat(str));
}

function resetInputs() {
    const elements = document.getElementsByTagName('input');
    for (let i = 0; i < elements.length; i++) {
        elements[i].value = '';
    }
    window.location.reload();
}
